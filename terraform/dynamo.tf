# aws_dynamodb_table
resource "aws_dynamodb_table" "temperature" {
  name = "temperature"
  hash_key = "id"
  attribute {
    name = "id"
    type = "S"
  }
    read_capacity = 1
    write_capacity = 1
}